package com.example.apibase.app.example.controller;

import com.example.apibase.domain.example.model.Example;
import com.example.apibase.domain.example.service.ExampleService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controllerクラスのサンプル.
 */
@RestController
@RequestMapping("/example")
public class ExampleController {

    private static Logger logger = LoggerFactory.getLogger(ExampleController.class);

    private ExampleService exampleService;

    public ExampleController(ExampleService exampleService) {
        this.exampleService = exampleService;
    }

    /**
     * /example または /example/ にアクセスされた時、Exampleのリストを返す.
     *
     * @return Exampleのリスト
     */
    @GetMapping
    public List<Example> index() {
        return exampleService.getExamples();
    }

    /**
     * /example/{id} にアクセスされた時、そのidのExampleを返す.
     *
     * @param id Exampleのid.整数値.
     * @return idで指定されたExample
     */
    @GetMapping("/{id}")
    public Example getExampleById(@PathVariable("id") int id) {
        logger.info("info id: {}", id);
        if (id == 0) {
            throw new RuntimeException("id is zero.");
        }
        return exampleService.getExampleById(id);
    }
}
