package com.example.apibase.infra.example.repository;

import com.example.apibase.domain.example.model.Example;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 * Repositoryクラスのサンプル.
 */
@Repository
public class ExampleRepositoryImpl implements ExampleRepository {

    /**
     * Exampleのリストを返す.
     *
     * @return Exampleのリスト
     */
    @Override
    public List<Example> getExamples() {
        List<Example> examples = new ArrayList<>();
        examples.add(new Example(1, "Hello"));
        examples.add(new Example(2, "Example"));
        examples.add(new Example(3, "Message"));
        return examples;
    }

    /**
     * 引数idで指定されたExampleを返す.
     *
     * @param id Exampleのid.整数値.
     * @return idで指定されたExample
     */
    @Override
    public Example getExampleById(int id) {
        return new Example(id, "Example");
    }
}
