package com.example.apibase.infra.example.repository;

import com.example.apibase.domain.example.model.Example;
import java.util.List;

/**
 * Repositoryインタフェースのサンプル.
 */
public interface ExampleRepository {

    /**
     * Exampleのリストを返す.
     *
     * @return Exampleのリスト
     */
    public List<Example> getExamples();

    /**
     * 引数idで指定されたExampleを返す.
     *
     * @param id Exampleのid.整数値.
     * @return idで指定されたExample
     */
    public Example getExampleById(int id);
}
