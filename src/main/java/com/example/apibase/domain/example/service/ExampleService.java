package com.example.apibase.domain.example.service;

import com.example.apibase.domain.example.model.Example;
import java.util.List;

/**
 * Serviceインタフェースのサンプル.
 */
public interface ExampleService {

    /**
     * Exampleのリストを返す.
     *
     * @return Exampleのリスト
     */
    public List<Example> getExamples();

    /**
     * 引数idで指定されたExampleを返す.
     *
     * @param id Exampleのid.整数値.
     * @return idで指定されたExample
     */
    public Example getExampleById(int id);
}
