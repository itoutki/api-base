package com.example.apibase.domain.example.service;

import com.example.apibase.domain.example.model.Example;
import com.example.apibase.infra.example.repository.ExampleRepository;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 * Serviceクラスのサンプル.
 */
@Service
public class ExampleServiceImpl implements ExampleService {

    private ExampleRepository exampleRepository;

    /**
     * コンストラクタ.
     *
     * @param exampleRepository ExampleRepository
     */
    public ExampleServiceImpl(ExampleRepository exampleRepository) {
        this.exampleRepository = exampleRepository;
    }

    /**
     * Exampleのリストを返す.
     *
     * @return Exampleのリスト
     */
    @Override
    public List<Example> getExamples() {
        return exampleRepository.getExamples();
    }

    /**
     * 引数idで指定されたExampleを返す.
     *
     * @param id Exampleのid.整数値.
     * @return idで指定されたExample
     */
    @Override
    public Example getExampleById(int id) {
        return exampleRepository.getExampleById(id);
    }
}
