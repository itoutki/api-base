package com.example.apibase.domain.example.model;

/**
 * Modelクラスのサンプル.
 */
public class Example {
    int id;
    String message;

    /**
     * コンストラクタ.
     *
     * @param id id
     * @param message message
     */
    public Example(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}
