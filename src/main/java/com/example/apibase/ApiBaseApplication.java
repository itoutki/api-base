package com.example.apibase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Bootアプリケーションのエントリポイント.
 */
@SpringBootApplication
public class ApiBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiBaseApplication.class, args);
    }

}
